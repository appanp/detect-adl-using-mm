# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This source code repository contains source code from:

1. Energia Sketch files provided in the [edX course EE40LX on Electronic Interfaces](https://courses.edx.org/courses/BerkeleyX/EE40LX)
1. Energia sketch files developed to solve the assignments for that course.
1. Source files developed for implementing the ADL tracking project.

### How do I get set up? ###

* TODO: Add later


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
